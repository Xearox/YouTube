<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<!doctype html>
<html>
<head>
	<title>Unsere Webseite</title>
</head>
<body>
	<form action="AddComment" method="get">
		<a>Username*</a><br>
		<input name="username" type="text" required="required" value="${sessionScope.username ne null ? sessionScope.username : '' }"><br>
		<a>E-Mail*</a><br>
		<input name="email" type="text" required="required"><br>
		<a>Kommentar*</a><br>
		<textarea name="comment" rows="10" cols="50" required="required"></textarea><br>
		<input type="submit">
	</form>
	<a>* Kennzeichnet Pflicht Felder</a>
	<br>
	<br>
	<br>
	<div>
		<c:forEach var="comment" items="${applicationScope.comments }">
			<a>Kommentar von: ${comment.getUsername() }</a><br>
			<a>${comment.getComment() }</a><br>
		</c:forEach>
	</div>
</body>
</html>