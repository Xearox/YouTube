package de.xearox.youtube.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de.xearox.youtube.comment.Comment;

/**
 * Servlet implementation class AddComment
 */
@WebServlet("/AddComment")
public class AddComment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddComment() {
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String username = request.getParameter("username");
		String email = request.getParameter("email");
		String comment = request.getParameter("comment");
		PrintWriter w = response.getWriter();
		ServletContext context = request.getServletContext();
		
		
		ArrayList<Comment> commentList = null;
		if(context.getAttribute("comments") == null) {
			commentList = new ArrayList<>();
		} else {
			commentList = (ArrayList<Comment>) context.getAttribute("comments");
		}
		
		if(username.equals("")) {
			w.write("Error:Username empty");
		}
		if(email.equals("")) {
			w.write("Error:Email empty");
		}
		if(comment.equals("")) {
			w.write("Error:Comment empty");
		}
		
		session.setAttribute("username", username);
		Comment c = new Comment();
		c.setUsername(username);
		c.setEmail(email);
		c.setComment(comment);
		
		commentList.add(c);
		context.setAttribute("comments", commentList);
		response.sendRedirect("http://localhost:8080/youtube/index.jsp");
	}

}
